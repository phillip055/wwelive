from application import app
from flask import request,jsonify, g
from application.utils.shows import *

@app.route("/wwe/<show_type>/update",methods=["GET"])
def getEpisodes(show_type):
	shows = getOldShows(show_type)
	return jsonify(shows)

@app.route('/wwe/<show_type>',methods=["GET"])
def getShows(show_type):
	offset = request.args.get('offset')
	limit = request.args.get('limit')
	shows = listShows(show_type,limit,offset)
	length = len(shows)
	true_shows = list()
	for show in shows:
		true_shows.append(show.to_mongo())
	return jsonify(shows=(true_shows),count=length)