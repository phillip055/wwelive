from flask import Flask,jsonify
from flask.ext.mongoengine import MongoEngine

app = Flask(__name__)
app.config.from_object('config')
db = MongoEngine(app)

@app.route("/")
@app.route('/wwe',methods=["GET"])
def status():
    return jsonify(status=True)

from application.controllers.shows import *