import requests
import string
from application.models.partialpart import *
from bs4 import BeautifulSoup
from application.models.show import *
from application.utils.customdatetime import *

def getOldShows(show_name):
	if show_name == "smackdown":
		full_url = "http://watchwrestling.in/sports/wwe-smackdown/"
	elif show_name == "raw":
		full_url = "http://watchwrestling.in/sports/wwe-raw-2/"
	elif show_name == "ppv":
		full_url = "http://watchwrestling.in/sports/wwe-ppv-2/"
	elif show_name == "nxt":
		full_url = "http://watchwrestling.in/sports/wwe-nxt/"
	elif show_name == "main_event":
		full_url = "http://watchwrestling.in/sports/wwe-main-event/"
	elif show_name == "ufc":
		full_url = "http://watchwrestling.in/sports/ufc/"
	elif show_name == "tna":
		full_url = "http://watchwrestling.in/sports/tna-impact-2/"
	else:
		return []
	shows = list()
	for i in range(1,200):
		paging = ""
		if i != 1:
			paging = "page/" + str(i)
		html = requests.get(full_url+paging,stream=True)
		soup = BeautifulSoup(html.content)
		try:
			content_soup = soup.find("div", {"id": "content"})
			links = content_soup.findAll("a",{"class":"clip-link"})
		except Exception, e:
			return shows
		dead = False
		for j in links:
			newShow = Show()
			newShow.name = string.join(j['title'].split()[1:-1])
			newShow.created_date = stringtoDt(j['href'].split('-')[-1][:-1])
			newShow.url = j['href']
			try:
				newShow.cover_picture = j.find('img')['src']
			except:
				pass
			newShow.updated_at = dt_now()
			if show_name != "ppv":
				newShow.isPPV = False
			else:
				newShow.isPPV = True
			newShow.show_type = show_name
			print  newShow.name + " " + newShow.url
			result = requests.get(newShow.url,stream=True)
			soup = BeautifulSoup(result.content)
			content = soup.find("div",{"class":"entry-content rich-content"})
			links = list()
			for i in content.findAll('a'):
				newPartialPart = PartialPart()
				newPartialPart.updated_at = dt_now()
				names = list()
				try:
					if i.string.strip() in names:
						newPartialPart.name = i.string.strip() + " (Back Up)"
					else:
						newPartialPart.name = i.string.strip()
					names.append(newPartialPart.name)
					video_id = i['href'].split('=')[-1]
					link_url = "http://www.dailymotion.com/video/" + video_id
					existance = requests.get(link_url)
					if existance.status_code == 200:
						newPartialPart.link_url = "http://www.dailymotion.com/embed/video/" + video_id
						print newPartialPart.name + " " + newPartialPart.link_url
					else:
						newPartialPart.link_url = ""
						newPartialPart.isWorking = False
					links.append(newPartialPart)
				except:
					print "SOMETHING"
			newShow.links = links
			if len(newShow.links) == 0:
				continue
			newShow.save()
			shows.append(newShow)
	return shows


def listShows(show_type,limit,offset):
	if show_type == "all":
		return Show.objects().order_by('-created_date').skip(int(offset)).limit(int(limit))
	return Show.objects(show_type=show_type).order_by('-created_date').skip(int(offset)).limit(int(limit))