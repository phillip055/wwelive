from application import db

class PartialPart(db.EmbeddedDocument):
	name = db.StringField()
	isWorking = db.BooleanField(default=True)
	updated_at = db.DateTimeField()
	link_url = db.StringField()