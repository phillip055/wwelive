from application import db
from application.models.partialpart import *

class Show(db.Document):
	name = db.StringField()
	show_type = db.StringField()
	cover_picture = db.StringField()
	created_date = db.DateTimeField()
	isPPV = db.BooleanField()
	url = db.StringField(unique=True,primary_key=True)
	links = db.ListField(db.EmbeddedDocumentField(PartialPart))
	updated_at = db.DateTimeField()